﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace RGSSHook
{
    static class NativeMethods
    {
        [DllImport("Kernel32", CharSet = CharSet.Unicode)]
        internal static extern uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, uint nSize, string lpFileName);

        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        internal static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("Kernel32")]
        internal static extern IntPtr LoadLibrary(string path);

        [DllImport("Kernel32")]
        internal static extern IntPtr GetModuleHandle(string path);
    }
}
