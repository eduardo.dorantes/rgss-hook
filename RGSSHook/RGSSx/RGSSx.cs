﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace RGSSHook
{
    class RGSSx
    {
        private delegate int RGSSEval(string code);
        private static RGSSEval eval;
        private static IntPtr DLLHandle;

        public static bool Eval(string code)
        {
            return eval.Invoke(code) == 0 ? true : false;
        }

        public static bool Initialize()
        {
            string RGSSLibrary = GetGameLibrary();

            if (string.IsNullOrEmpty(RGSSLibrary))
            {
                return false;
            }
            else
            {
                DLLHandle = NativeMethods.LoadLibrary(RGSSLibrary);
                if (NativeMethods.GetProcAddress(DLLHandle, "RGSSInitialize") != IntPtr.Zero)
                {
                    Main.RGSSVersion = 1;
                }
                else if (NativeMethods.GetProcAddress(DLLHandle, "RGSSInitialize2") != IntPtr.Zero)
                {
                    Main.RGSSVersion = 2;
                }
                else if (NativeMethods.GetProcAddress(DLLHandle, "RGSSInitialize3") != IntPtr.Zero)
                {
                    Main.RGSSVersion = 3;
                }
                else
                {
                    Main.RGSSVersion = 0;
                }
            }
            eval = (RGSSEval)LoadFunction<RGSSEval>(RGSSLibrary, "RGSSEval");
            return true;
        }

        public static string GetGameLibrary()
        {
            StringBuilder Result = new StringBuilder(0x100);
            NativeMethods.GetPrivateProfileString("Game", "Library", "", Result, 0x100, @".\Game.ini");
            return Result.ToString();
        }

        internal static Delegate LoadFunction<T>(string libraryPath, string functionName) =>
            Marshal.GetDelegateForFunctionPointer(NativeMethods.GetProcAddress(NativeMethods.LoadLibrary(libraryPath), functionName), typeof(T));
    }
}
