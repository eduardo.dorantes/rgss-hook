﻿using RGiesecke.DllExport;
using System;
using System.Runtime.InteropServices;

namespace RGSSHook
{
    public class Main
    {
        private static bool Initialized = false;
        public static int RGSSVersion = 0;
        private static string DLLVersion = "1.0";

        /// <summary>
        /// Carga la librería y la vincula a la instancia del juego.
        /// </summary>
        /// <returns>bool</returns>
        [DllExport("LoadLibrary", CallingConvention = CallingConvention.Cdecl)]
        public static bool LoadLibrary()
        {
            if (Main.Initialized)
                return true;

            if (RGSSx.Initialize())
            {
                Main.Initialized = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Devuelve el valor de la versión actual de la librería.
        /// </summary>
        /// <returns></returns>
        [DllExport("Version", CallingConvention = CallingConvention.Cdecl)]
        public static string Version()
        {
            return Main.DLLVersion;
        }

        /// <summary>
        /// Método de prueba, imprime un mensaje en la ventana del juego.
        /// </summary>
        [DllExport("test", CallingConvention = CallingConvention.Cdecl)]
        public static void Test()
        {
            if (!Main.Initialized)
                return;

            RGSSx.Eval("print '_test_';");
        }
    }
}
