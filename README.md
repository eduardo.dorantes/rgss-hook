## .NET Framework based RGSSx Hook

### Información
Debes contar con conocimiento de C# para usar este componente.

Para exponer una función esta debe:
- Estar en la clase *Main*
- Ser un método *estático*.
- Ser un método *público*.

#### Estructura
    [DllExport("TestFunction", CallingConvention = CallingConvention.Cdecl)]
    static public void TestFunction(){
    	Console.WriteLine("Hola mundo");
    }

En primer lugar tenemos el atributo de exportación, este toma 2 parámetros:
- Alias de la función: es el nombre que se utilizará desde Ruby para llamar la función.
- Atributo que especifica la convención para llamar métodos no administrados. (Usaremos siempre *Cdecl* )

Finalmente tenemos nuestra función, recordamos que debe ser estática y pública.

#### Integración con RPG Maker
El archivo RGSSHook.rb contiene código de ejemplo y la documentación necesaria
para utilizar funciones de nuestra librería.

Copia el contenido a un script encima de Main y coloca el archivo RGSSHook.dll
en la carpeta raíz de tu proyecto.

El ejemplo contiene la función RGSSHook.test, esta imprime un mensaje
en la ventana del juego.

Para ejecutarla simplemente llama RGSSHook.test