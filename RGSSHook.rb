%Q[
================================================================================
                    .NET Framework-based RGSS Hook Template
  ----------------------------------------------------------------------------
  Autor: Eduardo Dorantes (Drimer)
  ----------------------------------------------------------------------------
  ============================================================================
  
  El siguiente fragmento de código es un ejemplo de como integrar la instancia
  del juego a una librería dinámica externa para extender la funcionalidad del
  juego.
  
  Este es un procedimiento especialmente útil para realizar tareas que de otro
  modo resultarían demandantes para el juego, por ejemplo: procesamiento de
  gráficos.
  
  +¿Qué debo saber?
    Hacemos uso de ::Win32API para realizar la vinculación.
    Debes definir una constante por cada función que deseas llamar de la librería.
    
    -Estructura:
      Win32API.new(A, B, C, D*)
      A:  Nombre de la librería.
      B:  Función a llamar.
      C:  Parámetros que toma la función.
      D:  Parámetros que devuelve la función.
      
      
      Los parametros pueden ser los siguientes y dependen del tipo/valor:
        L:  Long
        I:  Integer
        P:  Pointer
        V:  Void
      
      La representación es en relación a la cantidad de valores.
    
      Ejemplo:
      DLL_SUMAR = Win32API.new('RGSSHook', 'sumar', 'II', 'I')
        
        Donde:
        El 3er parámetro ('II') indica que toma dos valores enteros y
        el 4to ('I') indica que devuelve un valor entero.
      
      -¿Qué pasa si quiero pasar, por ejemplo, una cadena de texto?
        Para todos los valores que no sean ni L o I, utiliza P.
        
    -¿Cómo llamo las funciones?
      A través del método ::call(*args)
      Si la función acepta parámetros estos se pasan en la misma llamada.
      
      Ejemplo:
      DLL_SUMAR.call(2, 3) #=> 5
      
================================================================================
]
#==============================================================================#
#                   .NET Framework-based RGSS Hook Template
#						Autor: Eduardo Dorantes (Drimer)
#==============================================================================#

module RGSSHook
  DLL_NAME      = "RGSSHook"
  DLL_TEST      = Win32API.new(DLL_NAME, 'test', '', '')
  DLL_VERSION   = Win32API.new(DLL_NAME, 'Version', '', 'P')
  
  def self.version
    return DLL_VERSION.call()
  end
  
  def self.test
    return DLL_TEST.call()
  end
end

Win32API.new(RGSSHook::DLL_NAME, 'LoadLibrary', '', '').call()